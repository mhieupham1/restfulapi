<?php
namespace App\Transformers;

use App\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformers extends TransformerAbstract{
    public function transform(Category $category){
        return [
            "id"=>$category->id,
            "title"=>$category->title,
            "body"=>$category->body,
            "created_at"=> $category->created_at,
            "updated_at"=>$category->updated_at
        ];
    }
}
