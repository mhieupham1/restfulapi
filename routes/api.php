<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
app('Dingo\Api\Transformer\Factory')->register('Category', 'CategoryTransformer');
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    $api->post('login', 'App\Http\Controllers\APILoginController@login');
    $api->group(['middleware' => 'jwt.auth'], function ($api) {
        $api->resource('category', 'App\Http\Controllers\CategoryController');
    });

});
//    $api->middleware('jwt.auth')->get('users', function () {
//        return auth('api')->user();
//    });
//    $api->resource('users',[
//        'middleware'=>'jwt.auth',
//        'uses'=>'App\Http\Controllers\CategoryController'
//    ]);
